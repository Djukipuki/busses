<?php

namespace App\Repositories;

use App\Models\Bus as Model;
use App\Models\Bus;

class BusRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getAll()
    {
        return $this->startConditions()->get();
    }

    public function getAllWithPaginate()
    {
        return $this->startConditions()->select()->orderBy('id','DESC')->paginate(10);
    }

    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    public function createNew()
    {
        return new Bus();
    }
}