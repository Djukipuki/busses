<?php

namespace App\Repositories;

use App\Models\Profession as Model;
use App\Models\Profession;


class ProfessionRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getAll()
    {
        return $this->startConditions()->get();
    }

    public function getAllWithPaginate($perpage)
    {
        return $this->startConditions()->select()->orderBy('id','DESC')->paginate($perpage);
    }


    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    public function createNew()
    {
        return new Profession();
    }
}