<?php

namespace App\Repositories;

use App\Models\Region as Model;
use App\Models\Region;


class RegionRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getAll()
    {
        return $this->startConditions()->get();
    }

    public function getAllWithPaginate()
    {
        return $this->startConditions()->select()->orderBy('id','DESC')->paginate(10);
    }


    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    public function createNew()
    {
        return new Region();
    }
}