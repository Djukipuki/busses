<?php

namespace App\Repositories;

use App\Models\Timetable as Model;
use App\Models\Timetable;
use Illuminate\Support\Facades\DB;

class TimetableRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getAllWithPaginate()
    {
        return $this->startConditions()->select()->orderBy('id','DESC')->paginate(10);
    }

    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    public function createNew()
    {
        return new Timetable();
    }

    public function getBusy($dates, $entity, $idEntity, $id)
    {
        $timetables = DB::table('timetables')
            ->where($entity, $idEntity)
            ->where(function ($query) use($dates) {
                $query->orWhereDate('departureTime', '=', $dates['departureTime'])
                    ->orWhereDate('departureTime', '=', $dates['arrivalTime'])
                    ->orWhereDate('arrivalTime', '=', $dates['departureTime'])
                    ->orWhereDate('arrivalTime', '=', $dates['arrivalTime']);
                });

        if($id){
            $timetables->where('id','<>', $id);
        }

        return $timetables->get(['departureTime', 'arrivalTime']);
    }

}
