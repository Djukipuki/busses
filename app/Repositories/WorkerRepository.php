<?php

namespace App\Repositories;

use App\Models\Worker as Model;
use Illuminate\Support\Facades\DB;
use App\Models\Worker;

class WorkerRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }

    public function getAll()
    {
        return $this->startConditions()->get();
    }

    public function getAllWithPaginate()
    {
        return $this->startConditions()->select()->orderBy('id','DESC')->paginate(10);
    }


    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    public function createNew()
    {
        return new Worker();
    }

    public function getDrivers()
    {
        return $drivers = DB::table('workers')
            ->join('professions', 'professions.id', '=', 'workers.professionId')
            ->where('professions.name', '=', 'Водій')
            ->select('workers.id', 'workers.firstName', 'workers.lastName')
            ->get();
    }
}