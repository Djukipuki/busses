<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timetable extends Model
{
    protected $fillable
        =[
            'bus_id',
            'route_id',
            'worker_id',
            'departureTime',
            'arrivalTime',
        ];

    public function bus()
    {
        return $this->belongsTo(Bus::class);
    }

    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    public function worker()
    {
        return $this->belongsTo(Worker::class);
    }
}