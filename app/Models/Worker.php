<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    protected $fillable=[
        'firstName',
        'lastName',
        'professionId',
        'phoneNumber',
        'homeAddress',
        'birthday'
    ];

    public function getProfessionName()
    {
        return $this->belongsTo(Profession::class, 'professionId', 'id');
    }
}
