<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable
        =[
            'name' ,
            'regionId'
        ];
    public function getRegionName()
    {
        return $this->belongsTo(Region::class, 'regionId', 'id');
    }
}
