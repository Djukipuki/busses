<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'model' => 'required',
            /*'carNumber' => 'required|integer|between:1000,9999',
            'seats' => 'required|integer|between:20,42',
            'fuelConsumption' => 'required|string|max:20',
            'typeOfTransportation' => 'required|string|max:50',
            'typeOfFuel' => 'required|string|max:20',
            'dateOfManufacture' => 'required|integer|between:1990,2020',
            'insuranceFrom' => 'required|date',
            'insuranceTo' => 'required|date',
            'run' => 'required|integer|between:0,500000',*/
        ];
    }
}
