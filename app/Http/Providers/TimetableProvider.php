<?php

namespace App\Http\Providers;

use App\Repositories\TimetableRepository;
use Carbon\Carbon;

class TimetableProvider
{
    protected $timetableRepository;

    public function __construct()
    {
        $this->timetableRepository = app(TimetableRepository::class);
    }

    public function isBusy($data, $entity)
    {
        $newDepartureTime = Carbon::createFromFormat('Y-m-d\TH:i', $data['departureTime']);
        $newArrivalTime = Carbon::createFromFormat('Y-m-d\TH:i', $data['arrivalTime']);

        $dates['departureTime'] = date('Y-m-d', strtotime($data['departureTime']));
        $dates['arrivalTime'] = date('Y-m-d', strtotime($data['arrivalTime']));

        $id  = $data['id'] ?? null;

        $timetables = $this->timetableRepository->getBusy($dates, $entity, $data[$entity], $id);

        foreach ($timetables as $timetable) {
            $busyDepartureTime = Carbon::createFromFormat('Y-m-d H:i:s', $timetable->departureTime);
            $busyArrivalTime = Carbon::createFromFormat('Y-m-d H:i:s', $timetable->arrivalTime);

            if($busyDepartureTime->between($newDepartureTime, $newArrivalTime)){
                return true;
            }

            if($busyArrivalTime->between($newDepartureTime, $newArrivalTime)){
                return true;
            }

            if($newDepartureTime->between($busyDepartureTime, $busyArrivalTime)){
                return true;
            }
        }

        return null;
    }
}