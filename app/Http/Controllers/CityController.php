<?php

namespace App\Http\Controllers;

use App\Repositories\CityRepository;
use App\Repositories\RegionRepository;
use Illuminate\Http\Request;

class CityController extends Controller
{
    protected $cityRepository;
    protected $regionRepository;

    public function __construct()
    {
        $this->cityRepository = app(CityRepository::class);
        $this->regionRepository = app(RegionRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = $this->cityRepository->getAllWithPaginate();
        $regions = $this->cityRepository->getAll();
        return view('cities', compact('cities', 'regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = $this->regionRepository->getAll();
        return view('cityAdd', compact( 'regions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        $this->cityRepository->createNew()->create($data);
        return redirect()->route('cities.index')->with('success', 'Населенный пункт сохранен');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = $this->cityRepository->getEdit($id);
        $regions = $this->regionRepository->getAll();
        return view('cityAdd', compact('city', 'regions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->input();
        $this->cityRepository->getEdit($id)->fill($data)->save();
        return redirect()->route('cities.index')->with('success', 'Населенный пункт сохранен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->cityRepository->getEdit($id)->forceDelete();
        return redirect()->route('cities.index')->with('success', 'Населенный пункт удален');
    }
}
