<?php

namespace App\Http\Controllers;

use App\Repositories\ProfessionRepository;
use Illuminate\Http\Request;

class ProfessionController extends Controller
{
    protected $professionRepository;

    public function __construct()
    {
        $this->professionRepository = app(ProfessionRepository::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professions = $this->professionRepository->getAllWithPaginate(10);
        return view('professions', compact('professions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('professionAdd');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        $this->professionRepository->createNew()->create($data);
        return redirect()->route('professions.index')->with('success', 'Дані збережено');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profession = $this->professionRepository->getEdit($id);
        return view('professionAdd', compact('profession'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->input();
        $this->professionRepository->getEdit($id)->fill($data)->save();
        return redirect()->route('professions.index')->with('success', 'Дані оновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->professionRepository->getEdit($id)->forceDelete();
        return redirect()->route('professions.index')->with('success', 'Дані видалено');
    }
}
