<?php

namespace App\Http\Controllers;

use App\Http\Requests\RouteRequest;
use App\Repositories\RouteRepository;


class RouteController extends Controller
{

    protected $routeRepository;

    public function __construct()
    {
        $this->routeRepository = app(RouteRepository::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = $this->routeRepository->getAllWithPaginate();

        return view('routes', compact('routes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function create(RouteRequest $request)
    {

    }

    public function store(RouteRequest $request)
    {
        $this->routeRepository->createNew()->create($request->input());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {

    }

    public function update(RouteRequest $request, $id)
    {
        $this->routeRepository->getEdit($id)->fill($request->input())->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->routeRepository->getEdit($id)->forceDelete();
    }
}