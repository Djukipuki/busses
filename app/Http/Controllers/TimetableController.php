<?php

namespace App\Http\Controllers;

use App\Http\Providers\TimetableProvider;
use App\Http\Requests\TimetableRequest;
use App\Repositories\BusRepository;
use App\Repositories\RouteRepository;
use App\Repositories\TimetableRepository;
use App\Repositories\WorkerRepository;
use Illuminate\Http\Request;

class TimetableController extends Controller
{
    const STARTING_CITY = 'Київ';

    protected $timetableProvider;
    protected $timetableRepository;
    protected $workerRepository;
    protected $routeRepository;
    protected $busRepository;

    public function __construct()
    {
        $this->timetableRepository = app(TimetableRepository::class);
        $this->workerRepository = app(WorkerRepository::class);
        $this->routeRepository = app(RouteRepository::class);
        $this->busRepository = app(BusRepository::class);
        $this->timetableProvider = app(TimetableProvider::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * **/

    public function index()
    {
        $timetables = $this->timetableRepository->getAllWithPaginate();
        $routes = $this->routeRepository->getAll();
        $buses = $this->busRepository->getAll();
        $drivers = $this->workerRepository->getDrivers();
        $startingCity = self::STARTING_CITY;
        return view('timetables', compact('timetables',  'startingCity', 'drivers', 'routes', 'buses'));
    }

    public function create(TimetableRequest $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TimetableRequest $request)
    {
        $this->timetableRepository->createNew()->create($request->input());
    }

    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TimetableRequest $request, $id)
    {
        $this->timetableRepository->getEdit($id)->fill($request->input())->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->timetableRepository->getEdit($id)->forceDelete();
    }

    public function checkBusyTime(Request $request)
    {
        $data = $request->input();
        $isDriverBusy = $this->timetableProvider->isBusy($data, 'worker_id');

        if($isDriverBusy){
            return '*Водій виконує маршрут в даний проміжку часу ';
        }
        $isBusBusy = $this->timetableProvider->isBusy($data, 'bus_id');

        if($isBusBusy){
            return '*Автобус курсує на іншому маршруті в даний проміжку часу';
        }
        return 'free' ;
    }
}
