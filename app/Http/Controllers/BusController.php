<?php

namespace App\Http\Controllers;

use App\Http\Requests\BusRequest;
use App\Repositories\BusRepository;

class BusController extends Controller
{
    protected $busRepository;

    public function __construct()
    {
        $this->busRepository = app(BusRepository::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buses = $this->busRepository->getAllWithPaginate();
        return view('buses', compact('buses'));
    }

    public function create(BusRequest $request)
    {

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BusRequest $request)
    {
        $bus = $this->busRepository->createNew()->create($request->input());
        if($bus){
            return redirect()
                ->route('buses.index')
                ->with(['success' => 'Успішно збережено']);
        }else{
            return back()
                ->withErrors(['msg' => 'Помилка збереження'])
                ->withInput();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {

    }

    public function update(BusRequest $request, $id)
    {
        $editBus = $this->busRepository->getEdit($id)->fill($request->input())->save();

        if($editBus){
            return redirect()
                ->route('buses.index')
                ->with(['success' => 'Успішно збережено']);
        }else{
            return back()
                ->withErrors(['msg' => 'Помилка збереження'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->busRepository->getEdit($id)->forceDelete();

    }
}
