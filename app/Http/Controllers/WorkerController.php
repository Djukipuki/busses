<?php

namespace App\Http\Controllers;

use App\Repositories\ProfessionRepository;
use App\Repositories\WorkerRepository;
use Illuminate\Http\Request;

class WorkerController extends Controller
{
    protected $professionRepository;
    protected $workerRepository;

    public function __construct()
    {
        $this->professionRepository = app(ProfessionRepository::class);
        $this->workerRepository = app(WorkerRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professions = $this->professionRepository->getAll();
        $workers = $this->workerRepository->getAllWithPaginate();
        return view('workers', compact('workers', 'professions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $professions = $this->professionRepository->getAll();
        return view('workerAdd', compact('professions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        $this->workerRepository->createNew()->create($data);
        return redirect()->route('workers.index')->with('success', 'Дані збережено');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $worker = $this->workerRepository->getEdit($id);
        $professions = $this->professionRepository->getAll();
        return view('workerAdd', compact('worker', 'professions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->input();
        $this->workerRepository->getEdit($id)->fill($data)->save();
        return redirect()->route('workers.index')->with('success', 'Дані оновлено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->workerRepository->getEdit($id)->forceDelete();
        return redirect()->route('workers.index')->with('success', 'Дані видалено');
    }
}
