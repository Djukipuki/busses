var $editBusTitle = $('#editBusTitle'),
    $editBusForm = $('#editBusForm'),
    $deleteBusForm = $('#deleteBusForm'),
    $editBusModal = $('#editBusModal'),
    urlBuses = 'buses';

$('#saveNewBusBtn').on('click',function () {
    $.ajax({
        type: 'POST',
        url: urlBuses,
        data: {
            model : $('#model').val(),
            carNumber : $('#carNumber').val(),
            seats : $('#seats').val(),
            fuelConsumption : $('#fuelConsumption').val(),
            typeOfTransportation : $('#typeOfTransportation').val(),
            typeOfFuel : $('#typeOfFuel').val(),
            dateOfManufacture : $('#dateOfManufacture').val(),
            insuranceFrom : $('#insuranceFrom').val(),
            insuranceTo : $('#insuranceTo').val(),
            run : $('#run').val(),
        },
        success: function (responce) {
            console.log(responce);
            $('#modalBus').modal('hide');
            location.reload();
        },
        error: function (error) {
            console.log(error);
        }
    });
});

$(document).on('click', '#editBus', function () {
    $editBusTitle.html('Редагування');
    $deleteBusForm.hide();
    $editBusForm.show();
    $('#idEdit').val($(this).data('id'));
    $('#model.edit').val($(this).data('model'));
    $('#carNumber.edit').val($(this).data('car_number'));
    $('#seats.edit').val($(this).data('seats'));
    $('#fuelConsumption.edit').val($(this).data('fuel_consumption'));
    $('#typeOfTransportation.edit').val($(this).data('type_of_transportation'));
    $('#typeOfFuel.edit').val($(this).data('type_of_fuel'));
    $('#dateOfManufacture.edit').val($(this).data('date_of_manufacture'));
    $('#insuranceFrom.edit').val($(this).data('insurance_from'));
    $('#insuranceTo.edit').val($(this).data('insurance_to'));
    $('#run.edit').val($(this).data('run'));
    $editBusModal.modal('show');
});

$('#updateBusBtn').on('click',  function () {
    var editId = $('#idEdit').val();
    $.ajax({
        type: 'PATCH',
        url: urlBuses + '/' + editId,
        data: {
            model : $('#model.edit').val(),
            carNumber : $('#carNumber.edit').val(),
            seats : $('#seats.edit').val(),
            fuelConsumption : $('#fuelConsumption.edit').val(),
            typeOfTransportation : $('#typeOfTransportation.edit').val(),
            typeOfFuel : $('#typeOfFuel.edit').val(),
            dateOfManufacture : $('#dateOfManufacture.edit').val(),
            insuranceFrom : $('#insuranceFrom.edit').val(),
            insuranceTo : $('#insuranceTo.edit').val(),
            run : $('#run.edit').val(),
        },
        success: function (responce) {
            console.log(responce);
            $('#editBusModal').modal('hide');
            location.reload();
        },
        error: function (error) {
            console.log(error);
        }
    });
});

$(document).on('click', '#deleteBus', function () {
    $editBusTitle.html('Видалення');
    $deleteBusForm.show();
    $editBusForm.hide();
    $editBusModal.modal('show');
    $('#idDelete').val($(this).data('id'));
});

$('#deleteBusBtn').on('click', function () {
    var idDelete = $('#idDelete').val();
    $.ajax({
        type: 'DELETE',
        url: urlBuses + '/' + idDelete,
        success: function (responce) {
            console.log(responce);
            $('#editBusModal').modal('hide');
            location.reload();
        },
        error: function (error) {
            console.log(error);
        }
    });
});