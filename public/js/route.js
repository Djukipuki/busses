
var $editRouteTitle = $('#editRouteTitle'),
    $editRouteModal = $('#editRouteModal'),
    $deleteRouteForm = $('#deleteRouteForm'),
    $editRouteForm = $('#editRouteForm'),
    utrRoutes = 'routes';

$('#saveNewRouteBtn').on('click',function () {
        $.ajax({
            type: 'POST',
            url: utrRoutes,
            data: $('#newRouteForm').serialize(),
            success: function (responce) {
                console.log(responce);
                $('#modalRoute').modal('hide');
                $('.modal-title').html('');
                location.reload();
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $(document).on('click', '#editRoute', function () {
        $editRouteTitle.html('Редагування');
        $deleteRouteForm.hide();
        $editRouteForm.show();
        $('#idEdit').val($(this).data('id'));
        $('#name.edit').val($(this).data('name'));
        $('#distance.edit').val($(this).data('distance'));
        $('#price.edit').val($(this).data('price'));
        $editRouteModal.modal('show');
    });

    $('#updateRouteBtn').on('click',  function () {
        var editId = $('#idEdit').val();
        $.ajax({
            type: 'PATCH',
            url: utrRoutes + '/' + editId,
            data: $editRouteForm.serialize(),
            success: function (responce) {
                console.log(responce);
                $('#editRouteModal').modal('hide');
                location.reload();
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $(document).on('click', '#deleteRoute', function () {
        $editRouteTitle.html('Видалення');
        $deleteRouteForm.show();
        $editRouteForm.hide();
        $editRouteModal.modal('show');

        $('#idDelete').val($(this).data('id'));
    });

    $('#deleteRouteBtn').on('click', function () {
        var idDelete = $('#idDelete').val();
        $.ajax({
            type: 'DELETE',
            url: utrRoutes + '/' + idDelete,
            success: function (responce) {
                console.log(responce);
                $('#editRouteModal').modal('hide');
                location.reload();
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

