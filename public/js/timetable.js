$(document).ready( function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

    var $newTimetableForm = $('#newTimetableForm'),
        $editTimetableForm = $('#editTimetableForm'),
        $deleteTimetableForm = $('#deleteTimetableForm'),
        $editTimetableModal = $('#editTimetableModal'),
        $editTimetableTitle = $('#editTimetableTitle'),
        urlTimetables = 'timetables';

    $newTimetableForm.on('keyup change', function () {
        var departureTime = $("#departureTime").val(),
            arrivalTime = $("#arrivalTime").val(),
            $result = $('#result');
        if(departureTime && arrivalTime) {
            var data = $newTimetableForm.serialize();
            $.post('check_busy_time', data, function (result) {
                if (result == 'free') {
                    $result.html('');
                    $('button[type=submit]').prop('disabled', false);
                } else {
                    $result.html(result);
                    $('button[type=submit]').prop('disabled', true);
                }
            })
        }
    });

    $('#saveNewTimetableBtn').on('click',function () {
        $.ajax({
            type: 'POST',
            url: urlTimetables,
            data: {
                route_id: $('select[name=route_id]').val(),
                bus_id: $('select[name=bus_id]').val(),
                worker_id: $('select[name=worker_id]').val(),
                departureTime: $('#departureTime').val(),
                arrivalTime: $('#arrivalTime').val()
            },
            success: function (responce) {
                console.log(responce);
                $('#modalTimetable').modal('hide');
                location.reload();
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $(document).on('click', '#editTimetable', function () {
        var bus_id = $(this).data('bus_id'),
            route_id = $(this).data('route_id'),
            driver_id = $(this).data('driver_id'),
            departureTime = $.format.date($(this).data('departure_time'), 'yyyy-MM-dd\THH:mm'),
            arrivalTime = $.format.date($(this).data('arrival_time'), 'yyyy-MM-dd\THH:mm');

        $editTimetableTitle.html('Редагування');
        $deleteTimetableForm.hide();
        $editTimetableForm.show();

        $('#idEdit').val($(this).data('id'));
        $('#bus_id.edit option[value = ' + bus_id + ']').prop('selected', true);
        $('#route_id.edit option[value = ' + route_id + ']').prop('selected', true);
        $('#worker_id.edit option[value = ' + driver_id + ']').prop('selected', true);
        $('#departureTime.edit').val(departureTime);
        $('#arrivalTime.edit').val(arrivalTime);
        $editTimetableModal.modal('show');
    });

    $editTimetableForm.on('keyup change', function () {
        var editDepartureTime = $("#departureTime.edit").val(),
            editArrivalTime = $("#arrivalTime.edit").val();
        if(editDepartureTime && editArrivalTime) {
            var data = $editTimetableForm.serialize();
            $.post('check_busy_time', data, function (result) {
                if (result == 'free') {
                    $('#result_edit').html('');
                    $('button[type=submit]').prop('disabled', false);
                } else {
                    $('#result_edit').html(result);
                    $('button[type=submit]').prop('disabled', true);
                }
            })
        }
    });

    $('#updateTimetableBtn').on('click', function () {
        var editId = $('#idEdit').val();
        $.ajax({
            type: 'PATCH',
            url: urlTimetables + '/' + editId,
            data: {
                route_id: $('select.edit[name=route_id]').val(),
                bus_id: $('select.edit[name=bus_id]').val(),
                worker_id: $('select.edit[name=worker_id]').val(),
                departureTime: $('#departureTime.edit').val(),
                arrivalTime: $('#arrivalTime.edit').val()
            },
            success: function (responce) {
                console.log(responce);
                $editTimetableModal.modal('hide');
                location.reload();
            },
            error: function (error) {
                console.log(error);
            }
        });
    });

    $(document).on('click', '#delete', function () {
        $editTimetableTitle.html('Видалення');
        $deleteTimetableForm.show();
        $editTimetableForm.hide();
        $editTimetableModal.modal('show');
        $('#idDelete').val($(this).data('id'));
    });

    $('#deleteTimetableBtn').on('click', function () {
        var idDelete = $('#idDelete').val();
        $.ajax({
            type: 'DELETE',
            url: urlTimetables + '/' + idDelete,
            success: function (responce) {
                console.log(responce);
                $editTimetableModal.modal('hide');
                location.reload();
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
});
