<?php

use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            ['name' => 'Винницкая'],
            ['name' => 'Волынская'],
            ['name' => 'Днепропетровская'],
            ['name' => 'Донецкая'],
            ['name' => 'Житомирская'],
            ['name' => 'Закарпатская'],
            ['name' => 'Запорожская'],
            ['name' => 'Ивано-Франковская'],
            ['name' => 'Киевская'],
            ['name' => 'Кировоградская'],
            ['name' => 'Луганская'],
            ['name' => 'Львовская'],
            ['name' => 'Николаевская'],
            ['name' => 'Одесская'],
            ['name' => 'Полтавская'],
            ['name' => 'Ровненская'],
            ['name' => 'Сумская'],
            ['name' => 'Тернопольская'],
            ['name' => 'Харьковская'],
            ['name' => 'Херсонская'],
            ['name' => 'Хмельницкая'],
            ['name' => 'Черкасская'],
            ['name' => 'Черниговская'],
            ['name' => 'Черновицкая']
        ];
        DB::table('regions')->insert($regions);

    }
}
