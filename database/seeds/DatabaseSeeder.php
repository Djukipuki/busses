<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RegionSeeder::class);
        $this->call(ProfessionSeeder::class);
        factory(App\Models\City::class, 35)->create();
        factory(App\Models\Worker::class, 50)->create();
        factory(App\Models\Bus::class, 10)->create();
        factory(App\Models\Route::class, 20)->create();
    }
}
