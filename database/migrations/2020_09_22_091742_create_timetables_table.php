<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimetablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetables', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('bus_id')->unsigned();
            $table->integer('route_id')->unsigned();
            $table->integer('worker_id')->unsigned();
            $table->dateTime('departureTime');
            $table->dateTime('arrivalTime');

            $table->timestamps();

            $table->foreign('bus_id')->references('id')->on('buses');
            $table->foreign('route_id')->references('id')->on('routes');
            $table->foreign('worker_id')->references('id')->on('workers');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timetables');
    }
}
