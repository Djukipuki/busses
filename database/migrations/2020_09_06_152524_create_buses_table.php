<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buses', function (Blueprint $table) {

            $table->increments('id');

            $table->string('model');
            $table->integer('carNumber');
            $table->integer('seats');
            $table->integer('fuelConsumption');
            $table->string('typeOfTransportation');
            $table->string('typeOfFuel');
            $table->integer('dateOfManufacture');
            $table->date('insuranceFrom');
            $table->date('insuranceTo');
            $table->integer('run');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('busses');
    }
}
