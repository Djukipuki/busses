<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Bus::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory :: create ( 'uk_UA' );

    return [
        'model' => $faker->word,
        'carNumber' => $faker->numberBetween(1000,9999),
        'seats' => $faker->numberBetween(20,38),
        'fuelConsumption' => $faker->numberBetween(9,18),
        'typeOfTransportation' => $faker->randomElement(['міський', 'міжміський']),
        'typeOfFuel' => $faker->randomElement(['бензин', 'дизель']),
        'dateOfManufacture' => $faker->numberBetween(1998,2018),
        'insuranceFrom' => $faker->date('Y:m:d', '2 years'),
        'insuranceTo' => $faker->date('Y:m:d', '-1 years'),
        'run' => $faker->numberBetween(50000,150000),
    ];
});