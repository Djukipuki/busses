<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Route::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory :: create ( 'uk_UA' );
    return [
        'name' => $faker->unique()->city,
        'distance' => $faker->numberBetween(10,450),
        'price' => $faker->numberBetween(20, 350),
    ];
});