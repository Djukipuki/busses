<?php
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$fakerRu = Faker\Factory::create('uk_UA');
$factory->define(App\Models\Worker::class, function (Faker\Generator $faker) use ($fakerRu) {

    return [
        'firstName' => $fakerRu->firstNameMale,
        'lastName' => $fakerRu->lastName,
        'professionId' => rand(1,5),
        'birthday'=> $faker->date($format = 'Y-m-d', $max = '2000-01-01'),
        'phoneNumber' => $faker->e164PhoneNumber,
        'homeAddress' => $fakerRu->address,
    ];
});

