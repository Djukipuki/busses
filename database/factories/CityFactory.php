<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$fakerRu = Faker\Factory::create('ru_RU');

$factory->define(App\Models\City::class, function (Faker\Generator $faker) use ($fakerRu){

    return [
        'name' => $fakerRu->city,
        'regionId' => rand(1,24),
    ];
});
