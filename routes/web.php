<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
})->name('home');

/*Route::get('/workerAdd', 'WorkerController@view')->name('worker_add');
Route::get('/professions', 'ProfessionController@getList')->name('professions');
Route::get('/profession_add', function () {
    return view('professionAdd');
})->name('profession_add');*/

/*Route::get('/profession_edit/{id}', 'ProfessionController@view')->name('profession_edit');
Route::get('/profession_delete/{id}', 'ProfessionController@delete')->name('profession_delete');
Route::post('/profession_update_submit/{id}', 'ProfessionController@update')->name('profession_update_submit');
Route::post('/profession_submit', 'ProfessionController@update')->name('profession_submit');*/

/*Route::get('/workers', 'WorkerController@getList')->name('workers');
Route::get('/worker_update/{id}', 'WorkerController@view')->name('worker_update');
Route::get('/worker_delete/{id}', 'WorkerController@delete')->name('worker_delete');
Route::post('/worker_update_submit/{id}','WorkerController@update')->name('worker_update_submit');
Route::post('/worker_submit', 'WorkerController@update')->name('worker_submit');*/

/*Route::get('/city_add', 'RegionController@getList')->name('city_add');*/

/*Route::get('/cities', 'CityController@getList')->name('cities');
Route::get('/city_update/{id}', 'CityController@view')->name('city_update');
Route::get('/city_delete/{id}', 'CityController@delete')->name('city_delete');
Route::post('/city_update_submit/{id}', 'CityController@update')->name('city_update_submit');
Route::post('/city_submit', 'CityController@submit')->name('city_submit');*/

Route::resource('workers', 'WorkerController');
Route::resource('professions', 'ProfessionController');
Route::resource('cities', 'CityController',
    ['except' => ['show']]);
Route::resource('routes', 'RouteController',
    ['except' => ['create', 'show', 'edit' ]]);
Route::resource('routes', 'RouteController',
    ['except' => ['create', 'show', 'edit' ]]);
Route::resource('timetables', 'TimetableController',
    ['except' => ['show']]);
Route::post('check_busy_time', 'TimetableController@checkBusyTime')->name('check_busy_time');
Route::resource('buses', 'BusController',
    ['except' => ['create', 'show', 'edit' ]]);
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
