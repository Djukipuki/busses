@extends('layouts.app')
@section('content')
    <div class="container">
        @if (isset($city))
            <form method="POST" action="{{ route('cities.update', $city->id)}}">
                {{ method_field('PATCH') }}
                @else
                    <form method="POST" action="{{ route('cities.store') }}">
                        @endif
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col">
                                <label for="name">Название</label>
                                <input type="text" name="name" class="form-control" id="name"
                                       value="{{ $city->name ?? '' }}">
                            </div>
                            <div class="col">
                                <label for="regionId">Область</label>
                                <select name="regionId" id="regionId" class="form-control">
                                    @if (isset($city))
                                        <option value="{{$city->regionId}}"
                                                selected>{{$city->getRegionName->name}}</option>
                                    @endif
                                    @foreach($regions as $region)
                                        <option value="{{ $region->id }}">{{$region->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>
                    @if (!isset($city))
                            <button type="submit" class="ml-3 btn btn-success">Добавить населенный пункт</button>
                    </form>
                    @else
                    <button type="submit" class="ml-3 btn btn-success">Сохранить населенный пункт</button>
            </form>

            <form method="POST" action="{{ route('cities.destroy', $city->id) }} ">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="submit" class="ml-3 btn btn-danger">Удалить населенный пункт</button>
            </form>
                    @endif

        <a href="{{ route('cities.index')}}">
            <button type="button" class="btn btn-primary">Назад</button>
        </a>
    </div>
@endsection