@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="d-flex justify-content-center">
            @if (isset($profession))
                <form class="form-inline" method="POST" action="{{ route('professions.update', $profession->id)}}">
                    {{ method_field('PATCH') }}
                    @else
                        <form class="form-inline" method="POST" action="{{ route('professions.store') }}">
                            @endif
                            {{ csrf_field() }}
                            <div class="form-group mx-sm-3 mb-2">
                                <label for="name" class="sr-only">Новая должность</label>
                                <input name="name" type="text" class="form-control" id="name"
                                       placeholder="Новая должность"
                                       @if(isset($profession))
                                       value="{{$profession->name ?? ''}}"
                                        @endif>
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">
                                @if(isset($profession))Сохранить
                                @else Добавить
                                @endif
                            </button>
                        </form>
        </div>
    </div>
@endsection