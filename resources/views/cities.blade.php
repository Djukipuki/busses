@extends('layouts.app')
@section('content')
    <div class="d-flex justify-content-left">
        <a href="{{ route('cities.create') }}">
            <button type="button" class=" ml-4 btn btn-primary">Добавить населенный пункт</button>
        </a>
    </div>
    <br>
    {{--<div class="container">
        <form class="form-inline" method="GET" action="{{ route('cities.index')}}">
            {{ csrf_field() }}
            <div class="form-group mx-sm-3 mb-2">
                <label for="region">Выберите регион </label>
                <select name="region" id="region" class="form-control">
                    @foreach($regions as $region)
                        <option value="{{ $region['id'] }}">{{$region['name']}}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary mb-2">Выбрать</button>
            <a href="{{ route('cities.index') }}">
                <button type="button" class="ml-3 btn btn-danger mb-2">Сбросить</button>
            </a>
        </form>
    </div>--}}

    <div class="container">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Місто</th>
                <th scope="col">Область</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($cities as $city)
                <tr>
                    <td>{{ $city->name }}</td>
                    <td>{{ $city->getRegionName->name }}</td>
                    <td>
                        <a href="{{ route('cities.edit', ['id' => $city->id] ) }}"> Подробнее </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

    <div class="d-flex justify-content-center">
        {{ $cities->links() }}
    </div>



@endsection