@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                    <div class="navbar navbar-toggleable-md navbar-light bg-faded">
                        <button type="button" class="btn btn-primary btn-sm" id="newTimetable" data-toggle="modal" data-target="#newTimetableModal">
                            Додати Запис
                        </button>
                        <a class="btn-sm btn-success" href="#">Історія маршруів</a>
                    </div>
                    
                    @include('modal.timetablesModal')

                    <table class="table table hover btn-sm" id="table">
                        <thead class="thead-light">
                        <tr>
                            <th>Маршрут</th>
                            <th>Автобус</th>
                            <th>Водій</th>
                            <th>Час віправвлення </th>
                            <th>Час прибуття</th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($timetables as $timetable)
                            <tr>
                                <td>{{ $startingCity . ' - ' . $timetable->route->name }}</td>
                                <td>{{ $timetable->bus->model . ' ' . $timetable->bus->carNumber }}</td>
                                <td>{{ $timetable->worker->firstName . ' ' . $timetable->worker->lastName }}</td>
                                <td>{{ $timetable->departureTime}}</td>
                                <td>{{ $timetable->arrivalTime}}</td>
                                <td>
                                    <a href="#" id="editTimetable" class="edit-modal btn btn-sm"
                                       data-id="{{$timetable->id}}"
                                       data-bus_id="{{$timetable->bus_id}}"
                                       data-route_id="{{$timetable->route_id}}"
                                       data-driver_id="{{ $timetable->worker_id }}"
                                       data-departure_time="{{ $timetable->departureTime }}"
                                       data-arrival_time="{{ $timetable->arrivalTime }}"> <i class="fa fa-pen"></i>
                                    </a>
                                    <a href="#" id="delete" class="delete-modal btn btn-sm" data-id="{{$timetable->id}}">
                                        <i style="color: #d12507" class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot></tfoot>
                    </table>
                     @if($timetables->total() > $timetables->count())
                         <div class="row justify-content-center">
                                         {{ $timetables->links() }}
                         </div>
                     @endif
                </div>
            </div>
        </div>
    </div>
@endsection