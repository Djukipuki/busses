@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="navbar navbar-toggleable-md navbar-light bg-faded">
                        <button type="button" class="btn btn-primary btn-sm" id="newRoute" data-toggle="modal" data-target="#newRouteModal">
                            Додати Запис
                        </button>
                    </div>
                    @include('modal.routeModal')
                        <table class="table table hover btn-sm" id="table">
                        <thead class="thead-light">
                        <tr>
                            <th>Назва маршруту</th>
                            <th>Відстань</th>
                            <th>Ціна </th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($routes as $route)
                            <tr>
                                <td>{{ $route->name }}</td>
                                <td>{{ $route->distance }}</td>
                                <td>{{ $route->price }}</td>
                                <td>
                                    <a href="#" id="editRoute" class="editRoute-modal btn btn-sm" data-id="{{$route->id}}"
                                       data-name="{{$route->name}}" data-distance="{{$route->distance}}"
                                       data-price="{{$route->price}}">
                                        <i  class=" fa fa-pen"></i>
                                    </a>
                                    <a href="#" id="deleteRoute" class="deleteRoute-modal btn btn-sm" data-id="{{$route->id}}">
                                        <i style="color: #d12507" class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot></tfoot>
                    </table>
                    @if($routes->total() > $routes->count())
                        <div class="row justify-content-center">
                            {{ $routes->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
