@extends('layouts.app')
@section('content')
    <div class="d-flex justify-content-left">
        <a href="{{ route('workers.create') }}">
            <button type="button" class="ml-4 btn btn-primary">Добавить работника</button>
        </a>
    </div>
    <br>
    <div class="container">
        <form class="form-inline" method="GET" action="{{ route('workers.index')}}">
            {{ csrf_field() }}
            <div class="form-group mx-sm-3 mb-2">
                <label for="profession">Выберите должность </label>
                <select name="profession" id="profession" class="form-control">
                    @foreach($professions as $profession)
                        <option value="{{ $profession['id'] }}">{{$profession['name']}}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary mb-2">Выбрать</button>
            <a href="{{ route('workers.index') }}">
                <button type="button" class="ml-3 btn btn-danger mb-2">Сбросить</button>
            </a>
        </form>
    </div>

    <div class="container">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Имя</th>
                <th scope="col">Фамилия</th>
                <th scope="col">Должность</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($workers as $worker)
            <tr>
                <td>{{ $worker->firstName }}</td>
                <td>{{ $worker->lastName }}</td>
                <td>{{ $worker->getProfessionName->name }}</td>
                <td>
                    <a href="{{ route('workers.edit', $worker->id) }}">
                    Подробнее
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

    </div>

    <div class="d-flex justify-content-center">
        {{ $workers->links() }}
    </div>


@endsection