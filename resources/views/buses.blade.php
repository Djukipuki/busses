@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                    <div class="navbar navbar-toggleable-md navbar-light bg-faded">
                        <button type="button" class="btn btn-primary btn-sm" id="newBus" data-toggle="modal" data-target="#newBusModal">
                            Додати Запис
                        </button>
                    </div>
                    @include('modal.busesModal')

                    <table class="table table hover btn-sm" id="table">
                            <thead class="thead-light">
                            <tr>
                            <th>Модель авто</th>
                            <th>Номерний знак</th>
                            <th>Кількість сидяч місць</th>
                            <th>Розхід топлива </th>
                            <th>Тип маршруту</th>
                            <th>Тип палива</th>
                            <th>Рік випуску авто</th>
                            <th>Дата страхування</th>
                            <th>Дата закінчення страхування</th>
                            <th>Пробіг авто</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($buses as $bus)
                            <tr>
                                <td><b>{{ $bus->model }}</b></td>
                                <td>{{ $bus->carNumber }}</td>
                                <td>{{ $bus->seats }}</td>
                                <td>{{ $bus->fuelConsumption }}</td>
                                <td>{{ $bus->typeOfTransportation}}</td>
                                <td>{{ $bus->typeOfFuel}}</td>
                                <td>{{ $bus->dateOfManufacture}}</td>
                                <td>{{ $bus->insuranceFrom}}</td>
                                <td>{{ $bus->insuranceTo}}</td>
                                <td>{{ $bus->run}}</td>
                                <td>
                                    <a href="#" id="editBus"
                                       class="editBus-modal btn btn-sm"
                                       data-id="{{$bus->id}}"
                                       data-model="{{ $bus->model }}"
                                       data-car_number="{{ $bus->carNumber }}"
                                       data-seats="{{ $bus->seats }}"
                                       data-fuel_consumption="{{ $bus->fuelConsumption }}"
                                       data-type_of_transportation="{{ $bus->typeOfTransportation }}"
                                       data-type_of_fuel="{{ $bus->typeOfFuel }}"
                                       data-date_of_manufacture="{{ $bus->dateOfManufacture }}"
                                       data-insurance_from="{{ $bus->insuranceFrom }}"
                                       data-insurance_to="{{ $bus->insuranceTo }}"
                                       data-run="{{ $bus->run }}">
                                        <i  class=" fa fa-pen"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="#" id="deleteBus" class="deleteBus-modal btn btn-sm"
                                       data-id="{{$bus->id}}">
                                        <i style="color: #d12507" class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot></tfoot>
                    </table>

                    @if($buses->total() > $buses->count())
                        <div class="row justify-content-center">
                            {{ $buses->links() }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
