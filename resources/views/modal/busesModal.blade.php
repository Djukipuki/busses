<div class="modal fade modalWindow" id="newBusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Новий запис</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form {{--method="POST" action="{{ route('buses.store') }}"--}} id="newBusForm">
                {{--{{ csrf_field() }}--}}

                <div class="modal-body">

                    <div>
                        <label for="model">Модель авто</label>
                        <input type="text" name="model" id="model" required>
                    </div>

                    <div>
                        <label for="carNumber">Номерний знак</label>
                        <input type="text" name="carNumber" id="carNumber" required>
                    </div>

                    <div>
                        <label for="seats">Кількість сидячих місць</label>
                        <input type="number" name="seats" id="seats" required>
                    </div>

                    <div>
                        <label for="fuelConsumption">Розхід топлива</label>
                        <input type="text" name="fuelConsumption" id="fuelConsumption" required>
                    </div>

                    <div>
                        <label for="typeOfTransportation">Тип маршруту</label>
                        <input type="text" name="typeOfTransportation" id="typeOfTransportation" required>
                    </div>

                    <div>
                        <label for="typeOfFuel">Тип палива</label>
                        <input type="text" name="typeOfFuel" id="typeOfFuel" required>
                    </div>

                    <div>
                        <label for="dateOfManufacture">Рік випуску авто</label>
                        <input type="number" name="dateOfManufacture" id="dateOfManufacture" required >
                    </div>

                    <div>
                        <label for="run">Пробіг авто</label>
                        <input type="number" name="run" id="run" required>
                    </div>

                    <div>
                        <label for="insuranceFrom">Дата страхування</label>
                        <input type="date" name="insuranceFrom" id="insuranceFrom" required >
                    </div>

                    <div>
                        <label for="insuranceTo">Дата закінчення страхування</label>
                        <input type="date" name="insuranceTo" id="insuranceTo" required>
                    </div>

                    <div id="result" style="color: #A52A2A" ></div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="submit" id="saveNewBusBtn" class="btn btn-success" >Зберегти</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{--EDIT TIMETABLE MODAL--}}
<div class="modal fade modalWindow" id="editBusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editBusTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editBusForm" {{--method="POST" action="{{ route('buses.update', ['id' => $bus->id  ] ) }}"--}} role="modal" >
               {{-- {{ method_field('PATCH') }}
                {{ csrf_field() }}--}}

                <div class="modal-body">
                    <input type="hidden" name="id" id="idEdit" >

                <div>
                    <label for="model">Модель авто</label>
                    <input class="edit" type="text" name="model" id="model" required>
                </div>

                <div>
                    <label for="carNumber">Номерний знак</label>
                    <input class="edit" type="text" name="carNumber" id="carNumber" required>
                </div>

                <div>
                    <label for="seats">Кількість сидячих місць</label>
                    <input class="edit" type="number" name="seats" id="seats" required>
                </div>

                <div>
                    <label for="fuelConsumption">Розхід топлива</label>
                    <input class="edit" type="text" name="fuelConsumption" id="fuelConsumption" required>
                </div>

                <div>
                    <label for="typeOfTransportation">Тип маршруту</label>
                    <input class="edit" type="text" name="typeOfTransportation" id="typeOfTransportation" required>
                </div>

                <div>
                    <label for="typeOfFuel">Тип палива</label>
                    <input class="edit" type="text" name="typeOfFuel" id="typeOfFuel" required>
                </div>

                <div>
                    <label for="dateOfManufacture">Рік випуску авто</label>
                    <input class="edit" type="number" name="dateOfManufacture" id="dateOfManufacture" required >
                </div>

                <div>
                    <label for="run">Пробіг авто</label>
                    <input class="edit" type="number" name="run" id="run" required>
                </div>

                <div>
                    <label for="insuranceFrom">Дата страхування</label>
                    <input class="edit" type="date" name="insuranceFrom" id="insuranceFrom" required >
                </div>

                <div>
                    <label for="insuranceTo">Дата закінчення страхування</label>
                    <input class="edit" type="date" name="insuranceTo" id="insuranceTo" required>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                <button type="submit" id="updateBusBtn" class="btn btn-primary">Редагувати</button>
            </div>
            </form>

            {{-- DELETE CONTENT--}}
            <form id="deleteBusForm" {{--action="{{ route( 'buses.destroy', ['id' => $bus->id]) }}" method="POST" --}}>
                <div class="modal-body">
                    <input type="hidden" name="id" id="idDelete" >
                    <div class="deleteContentBus"> Дійсно бажаєте видалити запис?<span class="hidden id"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="submit" id="deleteBusBtn" class="btn btn-danger">Видалити</button>
                </div>
            </form>

        </div>
    </div>
</div>