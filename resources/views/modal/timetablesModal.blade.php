@extends('layouts.app')
@section('content')
<div class="modal fade modalWindow" id="newTimetableModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Новий запис</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="newTimetableForm" >
                <div class="modal-body">
                    <div class="form-group">
                    <p>Маршрути :
                        <select class="field" id="route_id" name="route_id" required>
                            <option></option>
                            @foreach ($routes as $route)
                                <option value="{{ $route->id }}"> {{ $route->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    </div>

                    <div class="form-group">
                    <p>Автобуси :
                        <select class="field" id="bus_id" name="bus_id" required>
                            <option></option>
                            @foreach ($buses as $bus)
                                <option value="{{ $bus->id }}"> {{ $bus->model . ' ' . $bus->carNumber }}</option>
                            @endforeach
                        </select>
                    </p>
                    </div>

                    <div class="form-group">
                    <p>Водії:
                        <select class="field" id="worker_id" name="worker_id" required>
                            <option></option>
                            @foreach ($drivers as $driver)
                                <option value="{{ $driver->id }}"> {{ $driver->firstName . ' ' . $driver->lastName }}</option>
                            @endforeach
                        </select>
                    </p>
                    </div>

                    <div class="form-group">
                        <label for="departureTime">Час відправлення</label>
                        <input class="field" type="datetime-local" name="departureTime"
                               id="departureTime" min="{{ date('Y-m-d\Th:i') }}"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="arrivalTime">Час прибуття</label>
                        <input class="field" type="datetime-local" name="arrivalTime"
                               id="arrivalTime" min="{{ date('Y-m-d\Th:i') }}"
                               required>
                    </div>
                <div id="result" style="color: #A52A2A" ></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="submit" id="saveNewTimetableBtn" class="btn btn-success" >Зберегти</button>
                </div>
            </form>
        </div>
    </div>
</div>

                        {{--EDIT TIMETABLE MODAL--}}
<div class="modal fade modalWindow" id="editTimetableModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editTimetableTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editTimetableForm" role="modal">
                <div class="modal-body">
                    <input type="hidden" name="id" id="idEdit" >
                    <div class="form-group">
                        <p>Маршрути :
                            <select class="edit" id="route_id" name="route_id" required>
                                @foreach ($routes as $route)
                                    <option value="{{ $route->id }}"> {{ $route->name }}</option>
                                @endforeach
                            </select>
                        </p>
                    </div>

                    <div class="form-group">
                        <p>Автобуси :
                            <select class="edit" id="bus_id" name="bus_id" required>
                                @foreach ($buses as $bus)
                                    <option value="{{ $bus->id }}"> {{ $bus->model . ' ' . $bus->carNumber }}</option>
                                @endforeach
                            </select>
                        </p>
                    </div>

                    <div class="form-group">
                        <p>Водії:
                            <select class="edit" id="worker_id" name="worker_id" required>
                                @foreach ($drivers as $driver)
                                    <option value="{{ $driver->id }}"> {{ $driver->firstName . ' ' . $driver->lastName }}</option>
                                @endforeach
                            </select>
                        </p>
                    </div>

                    <div class="form-group">
                        <label for="departureTime">Час відправлення</label>
                        <input class="edit" type="datetime-local" name="departureTime"
                               id="departureTime" min="{{ date('Y-m-d\Th:i') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="arrivalTime">Час прибуття</label>
                        <input class="edit" type="datetime-local" name="arrivalTime"
                               id="arrivalTime" min="{{ date('Y-m-d\Th:i') }}" required>
                    </div>
                <div id="result_edit" style="color: #A52A2A" ></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                <button type="submit" id="updateTimetableBtn" class="btn btn-primary">Редагувати</button>
            </div>
            </form>
            {{-- DELETE CONTENT--}}
            <form id="deleteTimetableForm">
                <div class="modal-body">
                    <input type="hidden" name="id" id="idDelete" >
                    <div class="deleteContentTimetable"> Дійсно бажаєте видалити запис ?<span class="hidden id"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="submit" id="deleteTimetableBtn" class="btn btn-danger">Видалити</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection