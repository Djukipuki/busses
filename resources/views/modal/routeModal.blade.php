<div class="modal fade modalWindow" id="newRouteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Новий запис</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="newRouteForm">
            <div class="modal-body">
                    <div class="form-group">
                        <label  for="name">Назва маршруту</label>
                        <input class="route" type="text" name="name" id="name" placeholder="Назва" required>
                    </div>

                    <div class="form-group">
                        <label for="distance">Відстань</label>
                        <input class="route"  type="number" name="distance" id="distance"  placeholder="Відстань" required>
                    </div>

                    <div class="form-group">
                        <label  for="price">Ціна</label>
                        <input class="route" type="number" name="price" id="price"  placeholder="Ціна" required>
                    </div>
                <div id="result" style="color: #A52A2A" ></div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                <button type="submit" id="saveNewRouteBtn" class="btn btn-success">Зберегти</button>
            </div>

            </form>


        </div>
    </div>
</div>

{{--EDIT TIMETABLE MODAL--}}
<div class="modal fade modalWindow" id="editRouteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editRouteTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editRouteForm" role="modal">
            <div class="modal-body">
                    <input type="hidden" name="id" id="idEdit" >
                    <div class="form-group">
                        <label for="name">Назва маршруту</label>
                        <input class="edit" type="text" name="name" id="name" required>
                    </div>

                    <div class="form-group">
                        <label for="distance">Відстань</label>
                        <input class="edit" type="number" name="distance" id="distance" required>
                    </div>

                    <div class="form-group">
                        <label for="price">Ціна</label>
                        <input class="edit" type="number" name="price" id="price" required>
                    </div>
               {{-- <div id="result_edit" style="color: #A52A2A" ></div>--}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                <button type="submit" id="updateRouteBtn" class="btn btn-primary">Редагувати</button>
            </div>
            </form>

            {{-- DELETE CONTENT--}}
            <form id="deleteRouteForm">
                <div class="modal-body">
                    <input type="hidden" name="id" id="idDelete" >
                    <div class="deleteContentRoute"> Дійсно бажаєте видалити запис?<span class="hidden id"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="submit" id="deleteRouteBtn" class="btn btn-danger">Видалити</button>
                </div>

            </form>


        </div>
    </div>
</div>