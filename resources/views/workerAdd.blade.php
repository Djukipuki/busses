@extends('layouts.app')
@section('content')
    <div class="container">
        @if (isset($worker))
            <form method="POST" action="{{ route('workers.update', $worker->id)}}">
                {{ method_field('PATCH') }}
                @else
                    <form method="POST" action="{{ route('workers.store') }}">
                        @endif

                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="firstName">Имя</label>
                                <input type="text" name="firstName" class="form-control" id="firstName"
                                       value="{{ $worker->firstName ?? ''}}">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="lastName">Фамилия</label>
                                <input type="text" name="lastName" class="form-control" id="lastName"
                                       value="{{ $worker->lastName ?? ''}}">
                            </div>
                        </div>

                        <div class="form-row">


                            <div class="form-group col-md-6">
                                <label for="professionId">Должность</label>
                                <select name="professionId" id="professionId" class="form-control">
                                    @foreach($professions as $profession)
                                        <option value="{{ $profession->id }}"
                                        @if(isset($worker) && $profession->id == $worker->professionId ?? '')
                                            selected="selected"
                                                @endif
                                        >{{$profession->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <br>
                            <a href="{{ route('professions.index') }}">
                                <button type="button" class="mt-1 btn btn-primary">Редактировать должности</button>
                            </a>
                        </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="homeAddress">Адресс</label>
                                <input type="text" name="homeAddress" class="form-control" id="homeAddress"
                                       value="{{ $worker->homeAddress ?? '' }}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="phoneNumber">Номер телефона</label>
                                <input type="text" name="phoneNumber" class="form-control" id="phoneNumber"
                                       @if (isset($worker))
                                       value="{{ $worker->phoneNumber ?? '' }}">
                                @else
                                    value="380">
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="birthday">День рождения</label>
                                <input type="date" name="birthday" class="form-control" id="birthday"
                                       value="{{ $worker->birthday ?? '' }}">
                            </div>
                        </div>
                        <div class="d-flex justify-content-left">


                            @if (isset($worker))
                                <button type="submit" class="ml-3 btn btn-success">Сохранить работника</button>

                    </form>

                    <form action="{{ route('workers.destroy', $worker->id) }}" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                            <button type="submit" class="ml-3 btn btn-danger">Удалить работника</button>
                    </form>
                            @else
                                <button type="submit" class="ml-3 btn btn-primary">Добавить работника</button>
                            @endif
                        </div>
                    </form>
    <a href="{{ route('workers.index')}}">
        <button type="button" class="btn btn-primary">Назад</button>
    </a>
    </div>
@endsection