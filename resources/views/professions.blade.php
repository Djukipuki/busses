@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="d-flex justify-content-center">
            <a href="{{route('professions.create')}}">
                <button type="button" class="btn btn-primary mb-2">Додати</button>
            </a>
        </div>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Посади</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($professions as $profession)
                <tr>
                    <td>{{ $profession->name }}</td>
                    <td>
                        <a href="{{ route('professions.edit', $profession->id) }}">
                            Редагувати
                        </a>
                    </td>
                    <td>
                            <form action="{{ route('professions.destroy', $profession->id) }}" method="POST">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                           <button type="submit">Видалити</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection